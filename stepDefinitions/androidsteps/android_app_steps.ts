

import PlatformDomain from '../../pages/mobile_app/android/platform_domain_screen';
import Login from '../../pages/mobile_app/android/login_screen';
import Settings from '../../pages/mobile_app/android/settings_screen';
import StoreAssociate from '../../pages/mobile_app/android/store_associate_login_screen';


const {Given, When, Then} = require('cucumber');
import {expect} from 'chai';

browser.timeouts('implicit',8000);

Given(/^User opens Aptos Store Selling Android App and provided the Test Environment for Platform Domain as ([^"]*) and clicks on Done button$/, (platform_domain_env:string) =>{
  
    PlatformDomain.setPlatformDomain(platform_domain_env);

});

When(/^User provides valid credentials with ([^"]*) and ([^"]*) and clicks on Log in button$/, (username:string, password:string) =>
{
           Login.userLogin(username,password);
 
});

Then(/^should be able login successfully$/, () => {

        expect(Settings.isSettingsTitleDisplayed()).to.true;


});

When(/^User provides invalid credentials with ([^"]*) and ([^"]*) and clicks on Log in button$/, (username:string ,password:string) =>
{

            Login.userLogin(username,password);
 
});

Then(/^User should not be logged and should prompt an invalid username and password error message$/, () => {


            expect(Login.InvalidCredentialMessageIsDisplayed()).to.true;

            browser.reset();

});

Given(/^User Logged in successfully and verify the Setting page$/,() =>
{

          expect(Settings.isSettingsTitleDisplayed()).to.true;



});
        
When(/^User select Chicago retail Store and provides the Terminal Number as ([^"]*) and click on next transaction Number$/,(terminalNumber:string) =>
{


            Settings.clickOnSettingsDropdown();

            Settings.selectRetailStore();

            Settings.enterTerminalNumber(terminalNumber);


            Settings.clickOnNextTransactionNumber();

            Settings.clickOnDone();

           
            if(Settings.isRevokeOtherDevicePopupDisplyed)
            {
                Settings.clickOnRevokeOtherDeviceYes();
            }
});

Then(/^Validate Store Associate Login page$/, () => {


   StoreAssociate.isUsernameEditBoxDisplayed();

   browser.pause(10000);



});
