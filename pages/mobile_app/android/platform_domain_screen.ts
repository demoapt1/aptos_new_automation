


class PlatformDomainScreen
{


    private get platform_domain_edit_box ()
    {
        return  $(`android=new UiSelector().text("Platform domain").className("android.widget.EditText")`);
        
    }


    public setPlatformDomain(environment:string)
    {
        

        this.platform_domain_edit_box.setValue(environment +'\n');
    
    }

}

const PlatformDomain = new PlatformDomainScreen();

export default PlatformDomain;