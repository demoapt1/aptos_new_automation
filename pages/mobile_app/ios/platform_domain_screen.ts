


class PlatformDomainScreen
{

    private get dismiss_all_popup ()
    {
        return $('~Dismiss All');
    }

    private get platform_domain_edit_box ()
    {
        return $('(//XCUIElementTypeOther[@name="Platform domain"])[2]/XCUIElementTypeTextField');
    }

    private get done_button()
    {
        return $('~Done');
    }

    private dismissAll()
    {
        this.dismiss_all_popup.click();
    }

    private clickOnDone()
    {
        this.done_button.click();
    }

    public setPlatformDomain(environment:string)
    {
        this.dismissAll();

        this.platform_domain_edit_box.setValue(environment);

        this.clickOnDone();
    }

    


}

const PlatformDomain = new PlatformDomainScreen();

export default PlatformDomain;