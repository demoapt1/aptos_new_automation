


class StoreAssociateLoginScreen {

    private get dismiss_all_popup()
    {
        return $('~Dismiss All');
    }

    private get username_edit_box()
    {
        return $('~Username');
    };

    private get password_edit_box()
    {
        return $('~Password');
    }

    private get invalid_username_or_password_popup()
    {
        return $('(//XCUIElementTypeStaticText[@name="Invalid username or password"])[1]');
    }

    private get close_invalid_credential_popup()
    {
        return $('(//XCUIElementTypeOther[@name="Close"])[2]');
    }

    public isUsernameEditBoxDisplayed():boolean
    {
        return this.username_edit_box.isExisting();
    }

    private setUsername(username:string)
    {
        this.username_edit_box.setValue(username);
    }

    private setPassword(password:string)
    {
        this.password_edit_box.setValue(password + '\n');
    }

    public login(username:string, password:string)
    {
        
        

        this.setUsername(username);

        this.setPassword(password);
    }

    public isInvalidUsernameOrPasswordPopupDisplayed():boolean
    {
        return this.invalid_username_or_password_popup.isExisting();
    }

    public closeInvalidUsernameOrPasswordPopup()
    {
        this.close_invalid_credential_popup.click();
    }

    public dismissAll()
    {
        this.dismiss_all_popup.click();
    }

}

const StoreAssociate = new StoreAssociateLoginScreen();
export default StoreAssociate;