
const PropertiesReader = require('properties-reader');
const properties = PropertiesReader('./config/testrail.properties');

const Testrail = require('testrail-api');

let testrail = Testrail;

export interface IContentObject {
        assignedto_id: string;
        comment: string;
        defects: string;
        elapsed: string;
        status_id: number;
        version: string;
    }
export interface IAddTestRun {
    suite_id: string;
    assignedto_id: string;
    description: string;
    name: string;
    include_all: boolean;
    case_ids: any[];
    milestone_id: string;
}

export class IAddTestRunImpl implements IAddTestRun {
    public assignedto_id: string = '';
    public case_ids!: string[];
    public description: string = '';
    public include_all: boolean = true;
    public milestone_id: string = '';
    public name: string = '';
    public suite_id: string = '';
}

export class IContentObjectImpl implements IContentObject {
    public assignedto_id: string = '';
    public comment: string = '';
    public defects: string = '';
    public elapsed: string = '';
    public status_id: number = 0;
    public version: string = '';
}

export class TestRailreport {

    public constructor() {
    testrail = new Testrail({
        host: properties.get('hostname'),
        password: properties.get('password'),
        user: properties.get('username'),

    });
  }

  public  static testRunId:number = 0;
  public static set setRunId(runid: number){
      TestRailreport.testRunId = runid;
  }
  public static get getRunID(){
      return TestRailreport.testRunId;
  }
 public  async addTestResult(runid, caseid, content) {


       await testrail.addResultForCase(runid, caseid, content, (err, response, result) => {
         console.log('RESULT TTT' + err + response, result);
     });

    }

    public  async createTestrun(runName: string, suiteId: string) {

      //  let isrunpresent: boolean = false;
        console.log('CREAT TEST RUN');

      await  testrail.addRun(/*PROJECT_ID=*/properties.get('projectId'), /*CONTENT=*/{
            suite_id: suiteId,
            assignedto_id: '1',
            description: runName,
            name: runName,
            include_all: true,
            case_ids: [],
            milestone_id: properties.get('milestoneId'),
        },  (err, response, run) => {
            console.log(err+response+run);
        });

        console.log('RETURN ID NEW ' );
    }

    public  async getRunId(runName: string){

        console.log('getRunID() ----------'+ runName);

         await testrail.getRuns(/*PROJECT_ID=*/properties.get('projectId'), { }, function (err, response, runs) {
            console.log('RAIL----------'+err + response + 'KEYS _______________'+ runs.keys());

            for (const pair of runs.entries()) {

                for (var subpair of pair) {

                    console.log(typeof(subpair));
                    if(subpair instanceof Object == true) {
                        console.log('RUN NAME  ' + subpair.name);
                        console.log('RUN NAME ID ' + subpair.id);
                         if ( runName == subpair.name) {

                             TestRailreport.testRunId = subpair.id;
                             return subpair.id;
                        }
                    }
                }
            }
        });

     return TestRailreport.testRunId;
    }


}
